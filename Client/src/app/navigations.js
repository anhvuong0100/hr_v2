import ConstantList from "./appConfig";
export const navigations = [
  {
    name: "dashboard.Dashboard",
    // path:  ConstantList.ROOT_PATH+"dashboard/learning-management",
    icon: "home",
    path: ConstantList.ROOT_PATH+"dashboard/learning-management"
  },
  {
    name: "menu.organization_apparatus",
    icon: "format_list_bulleted",
    path:  ConstantList.ROOT_PATH+"crud-table",
    // badge: { value: "68", color: "primary" }
    children: [
      {
        name: "menu.organizational_structure",
        path: ConstantList.ROOT_PATH+"ecommerce/shop",
        icon: "supervisor_account",
        children: [
          {
            name: "menu.organizational_model",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.historical_organization",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          },
        
        ]
      },
      {
        name: "menu.payroll",
        path: ConstantList.ROOT_PATH+"ecommerce/cart",
        icon: "calendar_today",
        children: [
          {
            name: "menu.manage_staffing",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.payroll_management",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          },
        
        ]
      },
    
    ]
  },
  {
    name: "menu.salary",
    icon: "attach_money",
    children: [
      {
        name: "menu.records_management",
        path:  ConstantList.ROOT_PATH+"infinite-scroll",
        icon: "settings_applications",
        children: [
          {
            name: "menu.add_new",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.search",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          },
          {
            name: "menu.handing_over_records",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.number_of_days_allowed",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          },
          {
            name: "menu.lock_open",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.object_management_records",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          }

        ]
      },
      {
        name: "menu.manage_planning_results",
        path:  ConstantList.ROOT_PATH+"egret-list",
        icon: "category",
        children: [
          {
            name: "menu.planning_phase",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.manage_personnel",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          },
        
        ]
      },
      {
        name: "menu.manage_evaluation_results",
        icon: "settings_applications",
        path:  ConstantList.ROOT_PATH+"invoice/list"
      }
    ]
  },
  {
    name: "menu.refresher_training",
    icon: "list_alt",
    path: ConstantList.ROOT_PATH+"ecommerce/shop",
    children: [
      {
        name: "menu.Training_needs_tatistics",
        path: ConstantList.ROOT_PATH+"ecommerce/shop",
        icon: "supervisor_account",
        children: [
          {
            name: "menu.training_needs_by_title",
            path: ConstantList.ROOT_PATH+"ecommerce/shop",
            icon: "supervisor_account"
          },
          {
            name: "menu.training_needs_by_rank",
            path: ConstantList.ROOT_PATH+"ecommerce/cart",
            icon: "calendar_today"
          }
        
        ]
      },
      {
        name: "menu.approved",
        path: ConstantList.ROOT_PATH+"ecommerce/cart",
        icon: "calendar_today",
      
      },
      {
        name: "menu.decision_management",
        path: ConstantList.ROOT_PATH+"ecommerce/cart",
        icon: "calendar_today"
      },
    
    ]
  },
 
  {
    name: "menu.monitoring_inspection",
    icon: "category",
    // badge: { value: "50+", color: "secondary" },
    children: [
      {
        name: "menu.complaint_management",
        path:  ConstantList.ROOT_PATH+"material/autocomplete",
        icon: "subtitles"
      }
    ]
  },
  {
    name: "menu.decentralized_administration",
    icon: "settings",
    children: [
      {
        name: "menu.require_training",
        path:  ConstantList.ROOT_PATH+"others/pricing",
        icon: "supervised_user_circle",
        children: [
          {
            name: "menu.configure_plan_form",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "supervised_user_circle"
          },
          {
            name: "menu.configuration_requires_title",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "account_circle"
          },
          {
            name: "menu.configuration_requires_rank",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.program_Management_Training",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.staff_need_training",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.demand_training",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      },
      {
        name: "menu.category",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "account_circle",
        children: [
          {
            name: "menu.basic_catalog_system",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.job_category",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.unit_classification_catalog",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.contract_type",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.allowance_type",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.type_of_transfer",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.content_list",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.Regulations_payroll_bar",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.country_catalog",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.allowance_list",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.job_title",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.employees_ranks",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.reason_vacations",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      },
      {
        name: "menu.decentralized_management",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.system_menu",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.role_rights_group",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.decentralized_user_management",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]

      },
      {
        name: "menu.administrators",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.dynamic_form_management",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.report_management",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.dynamic_report",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.configuration_parameter_management",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.dynamic_report_configuration",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.dynamic_configuration",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.export_dynamic_reports",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.impact",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.system_resources",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.log_management",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      }
    ]
  },
  {
    name: "menu.statistic",
    icon: "list_alt",
    path: ConstantList.ROOT_PATH+"ecommerce/shop",
    children: [
      {
        name: "menu.report_form",
        path:  ConstantList.ROOT_PATH+"others/pricing",
        icon: "how_to_reg"
      },
      {
        name: "menu.report_on_demand",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.detailed",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.synthesis_structure",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      },
      {
        name: "menu.management_report",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg"
      }
    ]
  }
  ,
  {
    name: "menu.utility-support",
    icon: "list_alt",
    path: ConstantList.ROOT_PATH+"ecommerce/shop",
    children: [
      {
        name: "menu.warning_reminder",
        path:  ConstantList.ROOT_PATH+"others/pricing",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.lacks_records",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.appointment_term",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.discipline_to_raise_wages",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.retirement_age",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.rotation_staff",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.officers_due_to _aise_wages",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      },
      {
        name: "menu.import_data",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.import_new",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.import_update",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      },
      {
        name: "menu.looking_for_staff_information",
        path:  ConstantList.ROOT_PATH+"chart",
        icon: "how_to_reg",
        children: [
          {
            name: "menu.it_qualification_process",
            path:  ConstantList.ROOT_PATH+"others/pricing",
            icon: "how_to_reg"
          },
          {
            name: "menu.state_management_process_level",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
          ,
          {
            name: "menu.allowance process",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
          ,
          {
            name: "menu.disciplinary_process",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.qualification process",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.family_relationship",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.english_level",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.other_training_information",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.political reasoning level",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.participate_socio_political_organization",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.Bonus",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.salary_movements",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.social_insurance",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          },
          {
            name: "menu.go_on_bussiness",
            path:  ConstantList.ROOT_PATH+"chart",
            icon: "how_to_reg"
          }
        ]
      }
    ]
  }

];
