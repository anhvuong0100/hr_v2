import axios from "axios";
import HttpMethods from "./HttpService";
import ConstantList from "../appConfig";


const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization':'Basic Y29yZV9jbGllbnQ6c2VjcmV0'
        }
    }  
const  requestBody= {
    pageIndex:1,
    pageSize: 10
   } 
class BaseService {
    post=(url)=>{
       return axios.post(ConstantList.API_DATAFU+url,requestBody,config
          ).then(response=>{}).catch((error=>{
                    if(error.response.status === 401){ 
                        axios.post(ConstantList.API_DATAFU+'/oauth/token',{username:ConstantList.AUTH_NAME, password:ConstantList.AUTH_PASSWORD},{}
                        ).then((response) =>{
                            var token=response.data.token;
                            axios.post(ConstantList.API_DATAFU+url, requestBody,
                            { headers: {
                                         "Authorization" : `Bearer ${token}`,
                                          'Content-Type': 'application/json'
                                        }});
                        }).catch(
                            console.log(ConstantList.API_DATAFU+'/oauth/token '+ConstantList.AUTH_NAME+"-"+ ConstantList.AUTH_PASSWORD)    
                        );
                    }
                })    
         );
    }
}
export default new BaseService();