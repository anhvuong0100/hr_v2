/*
 * TA va Giang làm
 */

package com.globits.hr.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.globits.hr.domain.AcademicTitle;
import com.globits.hr.dto.AcademicTitleDto;

@Repository
public interface AcademicTitleRepository extends JpaRepository<AcademicTitle, Long>{
	@Query("select new com.globits.hr.dto.AcademicTitleDto(s) from AcademicTitle s")
	Page<AcademicTitleDto> getListPage( Pageable pageable);
}
