package com.globits.hr.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.globits.core.domain.BaseObject;

@XmlRootElement
@Table(name = "tbl_dowload_file")
@Entity
public class DowloadFile extends BaseObject {
	@Column(name = "name")
	private String name;
    
	@Column(name="title")
	private String title;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public DowloadFile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DowloadFile(String name, String title) {
		super();
		this.name = name;
		this.title = title;
	}

}
