package com.globits.hr.service;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.core.io.Resource;

import com.globits.hr.dto.DowloadFileDto;

public interface DowloadFileService {
	public Resource exportExcel(DowloadFileDto dowloadFile, HttpServletResponse response) throws IOException, InvalidFormatException;
}
