package com.globits.hr.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.globits.hr.dto.DowloadFileDto;
import com.globits.hr.service.DowloadFileService;
import com.globits.hr.utils.ImportExportExcelUtil;
@Service
public class DowloadFileServiceImpl implements DowloadFileService {
 
	@Autowired
	private ResourceLoader resourceLoader;
	@Override
	public Resource exportExcel(DowloadFileDto dowloadFileDto, HttpServletResponse response) throws IOException, InvalidFormatException  {
		try {
			if (dowloadFileDto != null) {
				List<DowloadFileDto> result = null;
				String filename = "DANH SÁCH CHƯƠNG TRÌNH ĐÀO TẠO";
				response.setContentType("application/vnd.ms-excel; charset=utf-8");
				response.setHeader("Content-Disposition", "attachment; filename=" + filename);
				response.setHeader("filename", filename);
				Resource resource = null;
				resource = resourceLoader.getResource(
						"classpath:" + "public/template/template_export/vi/" + "blankTemplate.xlsx");
				InputStream ip = resource.getInputStream();
				ByteArrayOutputStream bos = new ByteArrayOutputStream();

				ImportExportExcelUtil.exportExcelDowloadFile(result, ip, bos, filename);

				byte[] bytes = bos.toByteArray();
				ByteArrayResource res = new ByteArrayResource(bytes);
				bos.close();
				return res;
			}

			return null;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
			return null;
		}
	}

	

}
