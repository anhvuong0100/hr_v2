package com.globits.hr.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.globits.hr.dto.DowloadFileDto;
import com.globits.hr.service.DowloadFileService;

@RestController
public class RestDowloadFileController {

	@Autowired
	private DowloadFileService dowloadFileService;
	
	@PostMapping(value = "/api/dowloadfile")
	public Resource exportExcelTrainingProgram(@RequestBody DowloadFileDto dowloadFileDto, HttpServletResponse response) throws IOException, InvalidFormatException {
		try {
			return dowloadFileService.exportExcel(dowloadFileDto, response);
		} catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException e) {
			
			e.printStackTrace();
			return null;
		}
		
	}
	
}
